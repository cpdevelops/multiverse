// onload function
$(document).ready(function() {
    $("#aboutModal").iziModal({
        title: 'About',
        subtite: 'Information about this game',
        headerColor: '#a5c7ff'
    });
    $("#shipModal").iziModal();
    if (localStorage.getItem('gameState') === 'ON') {
        startGame();
    }
    else {
        back();
    }
});
// ship stuff
function shipName() {
    var input = document.getElementById("originalShipName").value;
    document.getElementById('shipName').innerHTML = input;
    $('#shipModal').iziModal('close');
    localStorage.setItem("shipName", input)
};
// start game button pressed
function startGame() {
    $('#game').show();
    $('#home').hide();
    localStorage.setItem('gameState', 'ON');
    if (localStorage.getItem('shipName') !== null) {
        document.getElementById('shipName').innerHTML = localStorage.getItem('shipName');;
    }
    else {
        $("#shipModal").iziModal("open");
    }
}
// back button pressed
function back() {
    $('#game').hide();
    $('#home').show();
}
function openTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}